package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class UIRegister extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uiregister);
    }

    public void onRegister(View view) {
        Intent intent = new Intent(this,UILogin.class);
        startActivity(intent);
    }
}
