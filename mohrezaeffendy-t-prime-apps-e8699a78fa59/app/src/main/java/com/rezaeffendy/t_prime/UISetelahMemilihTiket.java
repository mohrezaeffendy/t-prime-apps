package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class UISetelahMemilihTiket extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uisetelah_memilih_tiket);
    }

    public void PESAN(View view) {
        Intent intent = new Intent(this,UIUserTelahMemesanTiket.class);
        Toast.makeText(this, "Tiket berhasil dipesan", Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }
}
