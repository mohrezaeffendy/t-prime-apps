package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ListTiketInputUser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tiket_input_user);
    }

    public void LANJUTKAN1(View view) {
        Intent intent = new Intent(this,UISetelahMemilihTiket.class);
        startActivity(intent);
    }
}
