package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;

import android.util.Log;
import android.support.annotation.NonNull;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText etUsername, etPassword;
    String username, password;
    Button login,register;
    public static  final  String COLLECTION_NAME_KEY = "users";
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUsername = (EditText) findViewById(R.id.text_username);
        etPassword = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.btn_login);
        register = (Button) findViewById(R.id.btn_register);
        db = FirebaseFirestore.getInstance();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataUser();
                doLogin();
            }
        });
        register.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,Register.class));
            }
        });
    }

    private void getDataUser() {
        username = etUsername.getText().toString();
        password = etPassword.getText().toString();
    }

    private void doLogin() {
        if (!username.equals("") && !password.equals("")) {
            DocumentReference docRef = db.collection(COLLECTION_NAME_KEY).document(username);
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if (documentSnapshot.exists()) {
                        Users user = documentSnapshot.toObject(Users.class);
                        if (user.getPassword().equals(password)) {
                            Toast.makeText(getApplicationContext(), "welcome", Toast.LENGTH_SHORT).show();
//                            FirestoreCache cache = new FirestoreCache();
//                            cache.setCache(username);
                            startActivity(new Intent(Login.this,Dashboard.class));
//                            Log.d("T-PRIME LOGIN TAG", docRef.getId() + " => " + docRef.toString());

                        } else {
                            Toast.makeText(Login.this, "Passsword Mismatching", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Check your Username ", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else
        {
            Toast.makeText(Login.this, "Username or Password Cannot be Empty", Toast.LENGTH_SHORT).show();
        }
    }

}