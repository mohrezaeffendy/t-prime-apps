package com.rezaeffendy.t_prime;

import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private ListView mainListView ;
    private ArrayAdapter<String> listAdapter ;
    private GoogleMap mMap;
    String[] pool = new String[] {
            "Primajasa Pool Caringin",
            "Primajasa Pool Cawang"};
    String [] alamat= new String[]{
            "Jalan Soekarano Hatta No.181, Babakan Ciparay, Kota Bandung, Jawa Barat 40223",
            "Jalan Mayjen Sutoyo, Kramatjati, Kota Jakarta Timur"};
    //private AppLocationService appLocationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //listviewc
        customList customlist = new customList(this, pool, alamat);

        mainListView = (ListView) findViewById(R.id.mainListView);
        mainListView.setAdapter(customlist);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
       /* appLocationService = new AppLocationService(MapsActivity.this);
        Location newlocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);

        if (newlocation != null) {
            double curlat = newlocation.getLatitude();
            double curlog = newlocation.getLongitude();

            Log.d("latitude", curlat + "  ----" + curlog);*/

            // Add a marker in Sydney and move the camera
            LatLng cawang = new LatLng(-6.257558, 106.869233);
            LatLng caringin = new LatLng(-6.940132, 107.581972);
            mMap.addMarker(new MarkerOptions().position(cawang).title("Primajasa Pool Cawang"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(cawang));
            mMap.addMarker(new MarkerOptions().position(caringin).title("Primajasa Pool Caringin"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(caringin));

            /*LatLng coordinate = new LatLng(curlat, curlog);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 5);
            mMap.animateCamera(yourLocation);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(curlat, curlog))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }*/
    }
}
