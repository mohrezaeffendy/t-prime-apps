package com.rezaeffendy.t_prime;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;

public class CekBookingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_booking);
        SearchView search = (SearchView)findViewById(R.id.searchView);
        search.setQueryHint("Hint My Booking Code");
        search.setBackgroundColor(getResources().getColor(R.color.gray));
    }
}
