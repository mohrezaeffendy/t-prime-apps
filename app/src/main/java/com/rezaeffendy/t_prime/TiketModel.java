package com.rezaeffendy.t_prime;

public class TiketModel {
    private String kodeTiket;
    private String jamBerangkat;
    private String jamTiba;
    private int sisaKursi;
    private int harga;

    public TiketModel(String jamBerangkat, String jamTiba, int sisaKursi, int harga){
        this.jamBerangkat= jamBerangkat;
        this.jamTiba= jamTiba;
        this.sisaKursi= sisaKursi;
        this.harga= harga;
    }
    public String getKodeTiket() {
        return kodeTiket;
    }

    public void setKodeTiket(String kodeTiket) {
        this.kodeTiket = kodeTiket;
    }

    public String getJamBerangkat() {
        return jamBerangkat;
    }

    public void setJamBerangkat(String jamBerangkat) {
        this.jamBerangkat = jamBerangkat;
    }

    public String getJamTiba() {
        return jamTiba;
    }

    public void setJamTiba(String jamTiba) {
        this.jamTiba = jamTiba;
    }

    public int getSisaKursi() {
        return sisaKursi;
    }

    public void setSisaKursi(int sisaKursi) {
        this.sisaKursi = sisaKursi;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
}
