package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Profile extends AppCompatActivity {
    TextView tvNama,tvUsername,tvNoHp,tvIdentitas,tvAlamat;
    Button btnBackDashboard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        tvNama = (TextView) findViewById(R.id.textNama);
        tvUsername = (TextView) findViewById(R.id.textUsername);
        tvNoHp = (TextView) findViewById(R.id.textNoHp);
        tvIdentitas = (TextView) findViewById(R.id.textNoIdentitas);
        tvAlamat = (TextView) findViewById(R.id.textAlamat);
        btnBackDashboard = (Button) findViewById(R.id.buttonBack);
        btnBackDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Profile.this,Dashboard.class));
            }
        });
        getData();
    }
    protected void getData(){
        Users user = new Users();
        tvNama.setText(user.getNama());
        tvUsername.setText(user.getUsername());
        tvNoHp.setText(user.getNoHp());
        tvIdentitas.setText(user.getNoIdentitas());
        tvAlamat.setText(user.getAlamat());
    }
}
