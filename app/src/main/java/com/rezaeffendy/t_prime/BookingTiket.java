package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by Ganteng on 4/17/2018.
 */

public class BookingTiket extends AppCompatActivity {
   Button bookingTiket;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pesan_tiket);
        bookingTiket = (Button) findViewById(R.id.btnBooking) ;
        bookingTiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BookingTiket.this,Pembayaran.class));
            }
        });
    }
}
