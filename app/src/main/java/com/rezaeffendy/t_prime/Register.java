package com.rezaeffendy.t_prime;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {
    EditText textUsername,textNama,textPassword,textAlamat,textTelepon,textIdentitas;
    String username,namaLengkap,password,alamat,noHp,noIdentitas;
    private String USERNAME_KEY = "USERNAME";
    private String NAMA_KEY = "NAMA";
    private String PASSWORD_KEY = "PASSWORD";
    private String ALAMAT_KEY = "ALAMAT";
    private String HP_KEY = "NO_HP";
    private String ID_KEY = "NO_IDENTITAS";

    Button btnRegister;
    FirebaseFirestore db;
    public static  final  String COLLECTION_NAME_KEY = "users";
//    private static final String REGISTER_URL="http://rezaeffendy.com/tprime/userRegister.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        textUsername=(EditText)findViewById(R.id.text_username);
        textNama=(EditText)findViewById(R.id.text_fullname);
        textPassword=(EditText)findViewById(R.id.password);
        textAlamat=(EditText)findViewById(R.id.alamat);
        textTelepon=(EditText)findViewById(R.id.telepon);
        textIdentitas=(EditText)findViewById(R.id.identitas);
        btnRegister=(Button)findViewById(R.id.btn_register);
        db = FirebaseFirestore.getInstance();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataUser();
                doRegister();
//                addNewUser();
            }
        });
    }

    private void getDataUser() {
         namaLengkap = textNama.getText().toString();
         username = textUsername.getText().toString();
         password = textPassword.getText().toString();
         alamat = textAlamat.getText().toString();
         noHp = textTelepon.getText().toString();
         noIdentitas = textIdentitas.getText().toString();
    }

//    private void addNewUser(){
//        Map<String, Object>newUser = new HashMap<>();
//        newUser.put(USERNAME_KEY,username);
//        newUser.put(NAMA_KEY,namaLengkap);
//        newUser.put(PASSWORD_KEY,password);
//        newUser.put(ALAMAT_KEY,alamat);
//        newUser.put(HP_KEY,noHp);
//        newUser.put(ID_KEY,noIdentitas);
//        db.collection("users").add(newUser).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//            @Override
//            public void onSuccess(DocumentReference documentReference) {
//                Toast.makeText(Register.this, "User Berhasil Ditambahkan", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(Register.this,Login.class));
//                Log.d("FirestoreDemo", "DocumentSnapshot added with ID: " + documentReference.getId());
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(Register.this, "User Gagal Ditambahkan", Toast.LENGTH_SHORT).show();
//                Log.w("FirestoreDemo", "Error adding document", e);
//            }
//        });
//    }
    private void doRegister(){
        if (! namaLengkap.equals("")
                && ! username.equals("")
                && ! password.equals("")
                && ! alamat.equals("")
                && ! noHp.equals("")
                && ! noIdentitas.equals(""))
        {
                CollectionReference userRef = db.collection(COLLECTION_NAME_KEY);
                final Users users = new Users();
                users.setNama(namaLengkap);
                users.setUsername(username);
                users.setPassword(password);
                users.setAlamat(alamat);
                users.setNoHp(noHp);
                users.setNoIdentitas(noIdentitas);
                db.collection(COLLECTION_NAME_KEY).document(textUsername.getText().toString()).set(users);
                DocumentReference docRef = db.collection(COLLECTION_NAME_KEY).document();
                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists())
                        {

                                   /* Users users = documentSnapshot.toObject(Users.class);

                                    if (users.getName().equals(email.getText().toString()))
                                    {
                                        Toast.makeText(MainActivity1.this, "All ready registered", Toast.LENGTH_SHORT).show();
                                    }
*/
                            Toast.makeText(Register.this, "All ready registered", Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            users.setNama(namaLengkap);
                            users.setUsername(username);
                            users.setPassword(password);
                            users.setAlamat(alamat);
                            users.setNoHp(noHp);
                            users.setNoIdentitas(noIdentitas);
                            db.collection(COLLECTION_NAME_KEY).document(username).set(users);
                            Toast.makeText(Register.this, "registration successfull", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Register.this,Login.class));
                        }
                    }
                });
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Make All field Filled", Toast.LENGTH_SHORT).show();
        }

    }
}
