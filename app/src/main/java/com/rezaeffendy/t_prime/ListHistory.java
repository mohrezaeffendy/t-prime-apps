package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;

public class ListHistory extends RecyclerView.Adapter<ListHistory.historyView> {

    private final LinkedList<String> judul;
    private final LinkedList<Integer> image;

    private LayoutInflater inflater;//infalter untuk java objek dari layout

    public ListHistory(History history, LinkedList<Integer> nGambar, LinkedList<String> nHistory, LinkedList<String> nTujuan, LinkedList<String> nAsal, LinkedList<Integer> nKodeTiket, LinkedList<String> nNma, LinkedList<String> nNamaPenumpang, LinkedList<String> nStatus, LinkedList<String> nStatuOrder, LinkedList<String> nHarga, LinkedList<String> nJumlah, LinkedList<String> nTanggal, LinkedList<String> nWaktu) {
        inflater = LayoutInflater.from(history);
        this.judul = nHistory;
        this.image = nGambar;
    }

    @Override
    public historyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View listView = inflater.inflate(R.layout.activity_list_history, parent, false);
        return new historyView(listView, this);
    }

    @Override
    public void onBindViewHolder(ListHistory.historyView holder, int position) {
        String Currentjudul = judul.get(position);
        Integer Currentimage = image.get(position);

        holder.Mjuduls.setText(Currentjudul);
        holder.Mimages.setImageResource(Currentimage);
    }

    @Override
    public int getItemCount() {
        return image.size();
    }

    public class historyView extends RecyclerView.ViewHolder {
       private final TextView Mjuduls;
       private final ImageView Mimages;

        public historyView(final View listView, ListHistory listHistory) {
            super(listView);
            Mjuduls = listView.findViewById(R.id.Historyy);
            Mimages = listView.findViewById(R.id.gambar);

            Mimages.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int position = getLayoutPosition();

                    String jdl = judul.get(position);
                    Integer img = image.get(position);

                    Intent intent = new Intent(listView.getContext(),DetailHistory.class);

                    intent.putExtra("judul",jdl);
                    intent.putExtra("Gambar",img);

                    listView.getContext().startActivity(intent);


                }
            });


        }

    }
}
