package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.icu.text.AlphabeticIndex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

public class PesanTiket extends AppCompatActivity {
    Spinner poolAsal,poolTujuan,jumlahTiket;
    EditText tglBerangkat;
    Button cariTiket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cari_tiket);
        poolAsal = (Spinner) findViewById(R.id.spinnerKotaAsal);
        poolTujuan = (Spinner) findViewById(R.id.spinnerKotatujuan);
        jumlahTiket = (Spinner) findViewById(R.id.spinnerJumlahTiket);
        tglBerangkat = (EditText) findViewById(R.id.etTanggal);
        cariTiket = (Button) findViewById(R.id.btnCariTiket);
        cariTiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PesanTiket.this,ListTiket.class));
            }
        });
    }
}
