package com.rezaeffendy.t_prime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class CekBooking extends AppCompatActivity {
    Button btnCekBooking;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_booking);
        btnCekBooking = (Button) findViewById(R.id.btn_caribooking);
        btnCekBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CekBooking.this,HasilCekBookingActivity.class));
            }
        });
    }
}
