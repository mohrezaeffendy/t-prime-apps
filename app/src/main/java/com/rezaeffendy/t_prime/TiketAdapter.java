package com.rezaeffendy.t_prime;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class TiketAdapter extends RecyclerView.Adapter<TiketAdapter.TiketViewHolder> {
    private Context mCtx;
    private List<TiketModel> tiketModelList;

    AdapterView.OnItemClickListener mItemClickListener;
    public TiketAdapter(Context mCtx, List<TiketModel> tiketModelList){
        this.mCtx = mCtx;
        this.tiketModelList=tiketModelList;
    }

    @Override
    public TiketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.list_tiket, null);
        return new TiketViewHolder(view);
    }


    @Override
    public void onBindViewHolder(TiketAdapter.TiketViewHolder holder, int position) {
        TiketModel tiketModel = tiketModelList.get(position);
        holder.textViewBerangkat.setText(String.valueOf(tiketModel.getJamBerangkat()));
        holder.textViewTiba.setText(String.valueOf(tiketModel.getJamTiba()));
        holder.textViewKursi.setText(String.valueOf(tiketModel.getSisaKursi()));
        holder.textViewHarga.setText(String.valueOf(tiketModel.getHarga()));
    }

    @Override
    public int getItemCount() {
        return tiketModelList.size();
    }

    public class TiketViewHolder extends RecyclerView.ViewHolder {
        TextView textViewBerangkat, textViewTiba, textViewKursi, textViewHarga;
        Button btnPilih;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.list_tiket, null);
        public TiketViewHolder(final View itemView) {
            super(itemView);
            textViewBerangkat = (TextView)view.findViewById(R.id.tvJamBerangkat);
            textViewTiba= (TextView)view.findViewById(R.id.tvJamTiba);
            textViewKursi = (TextView)view.findViewById(R.id.tvSisaKursi);
            textViewHarga = (TextView)view.findViewById(R.id.tvHargaTiket);
            btnPilih = (Button)view.findViewById(R.id.btnLanjutkan);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                int position = getLayoutPosition();
                String berangkat = String.valueOf(tiketModelList.get(position));
                Intent intent = new Intent(itemView.getContext(),Pembayaran.class);
                intent.putExtra("berangkat",berangkat);

                    itemView.getContext().startActivity(intent);

                }
            });

//            btnPilih.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    startActivity()
//                }
//            });
        }

    }

}
